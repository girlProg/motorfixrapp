import React from 'react'
import {View, TouchableOpacity, Image, StyleSheet, Text} from "react-native"


const BreadcrumbsNav = ({navigation, screenName, withbackArrow}) => {
    return  <View style={styles.navbreadcrumbsviewView}>
			{ withbackArrow ? 
				<TouchableOpacity onPress={()=>{navigation.goBack()}}>
                        <Image source={require("./../../assets/images/back-icon.png")} style={styles.backIconImage}/>
				</TouchableOpacity> :
				null
			}
            <Text style={styles.breadcrumbtitleText}>{screenName}</Text>
            </View>
}


export default BreadcrumbsNav

const styles = StyleSheet.create({
    navbreadcrumbsviewView: { 
		height: 58,
		backgroundColor: "#ffffff",
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowOffset: {
			width: 0,
			height: 4
		},
		shadowRadius: 8,
		shadowOpacity: 1,
		marginBottom: 15,
		elevation: 2,
	},
	backIconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		position: "absolute",
		left: 25,
		right: 328,
		top: 21,
		height: 16,
	},
	breadcrumbtitleText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		position: "absolute",
		alignSelf: "center",
		top: 18,
	},
})