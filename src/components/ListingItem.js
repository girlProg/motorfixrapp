import React, { Component } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import Svg, { Path } from "react-native-svg";


const ListingItem = (props) => {
  return (
    <View style={[styles.root, props.style]}>
    <View style={styles.group26Stack}>
      <View style={styles.group26}>
        <View style={styles.itemListLine2}>
          <View style={styles.itemListLine3Stack}>
            <View style={styles.itemListLine3}>
              <View style={styles.itemListLine4}>
                <View style={styles.itemList3}>
                  <View style={styles.itemList4}>
                    <View style={styles.group7}>
                      <View style={styles.image2Row}>
                        <View style={styles.image2}>
                          <View style={styles.rectangle22}>
                          <Image
                                source={require("../../assets/images/rectangle-2.png")}
                                style={styles.productimageImage}/>
                          </View>
                        </View>
                        <View style={styles.alternatorBelt2Column}>
                          <Text style={styles.alternatorBelt2}>
                            Rear View Mirror
                          </Text>
                          <Text style={styles.captionHere}>Caption here</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <Svg
                  viewBox="-1 -1 378.4999999999999 6"
                  style={styles.lineCopy21}
                >
                  <Path
                    strokeWidth={2}
                    fill="rgba(216,216,216,1)"
                    stroke="rgba(251,252,254,1)"
                    fillOpacity={1}
                    strokeOpacity={1}
                    d="M1.00 2.00 L375.50 2.00 "
                  ></Path>
                </Svg>
              </View>
            </View>
            <View style={styles.group311}>
              <View style={styles.group45}>
                <Text style={styles.numbers7}>NEW</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <Image
        source={require("../../assets/images/edit-2.png")}
        resizeMode="contain"
        style={styles.image3}
      ></Image>
    </View>
  </View>
);
}


export default ListingItem;

const styles = StyleSheet.create({
    productimageImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 90,
		height: 90,
	},
root: {
  flex: 1
},
group26: {
  top: 0,
  left: 0,
  position: "absolute",
  opacity: 1,
  right: 0,
  bottom: 0
},
itemListLine2: {
  opacity: 1,
  flex: 1
},
itemListLine3: {
  top: 0,
  left: 0,
  height: 106,
  position: "absolute",
  opacity: 1,
  right: 0
},
itemListLine4: {
  opacity: 1,
  flex: 1
},
itemList3: {
  width: 268,
  height: 90,
  opacity: 1,
  marginLeft: 15
},
itemList4: {
  width: 268,
  height: 90,
  opacity: 1
},
group7: {
  width: 268,
  height: 90,
  opacity: 1
},
image2: {
  width: 104,
  height: 90,
  opacity: 1
},
rectangle22: {
  width: 104,
  height: 90,
  backgroundColor: "rgba(242,242,242,1)",
  borderRadius: 12
},
placeholdersVectorImage70Px14: {
  width: 81,
  height: 70,
  backgroundColor: "transparent",
  opacity: 1,
  marginTop: 10,
  marginLeft: 12
},
alternatorBelt2: {
  backgroundColor: "transparent",
  color: "rgba(6,6,6,1)",
  opacity: 1,
  fontSize: 16,
  fontFamily: "Avenir-Heavy",
  lineHeight: 20
},
captionHere: {
  backgroundColor: "transparent",
  color: "rgba(171,171,171,1)",
  opacity: 1,
  fontSize: 12,
  lineHeight: 16,
  letterSpacing: -0.2,
  marginTop: 6
},
alternatorBelt2Column: {
  width: 131,
  marginLeft: 17,
  marginTop: 6,
  marginBottom: 42
},
image2Row: {
  height: 90,
  flexDirection: "row",
  marginRight: 16
},
lineCopy21: {
  width: 378,
  height: 6,
  backgroundColor: "transparent",
  borderColor: "transparent",
  marginTop: 13,
  marginLeft: -1
},
group311: {
  top: 8,
  left: 289,
  width: 66,
  height: 22,
  position: "absolute",
  opacity: 1,
  shadowOffset: {
    height: 1,
    width: 0
  },
  shadowColor: "rgba(0,0,0,0.1)",
  shadowOpacity: 1,
  shadowRadius: 4
},
group45: {
  width: 66,
  height: 22,
  opacity: 1
},
numbers7: {
  width: 44,
  height: 12,
  backgroundColor: "transparent",
  color: "rgba(6,6,6,1)",
  opacity: 1,
  fontSize: 9,
  lineHeight: 12,
  letterSpacing: 0.15,
  marginTop: 5,
  marginLeft: 22
},
itemListLine3Stack: {
  height: 106,
  marginTop: 1
},
image3: {
  top: 64,
  left: 331,
  width: 26,
  height: 26,
  position: "absolute"
},
group26Stack: {
  flex: 1
}
});

