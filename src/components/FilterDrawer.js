//
//  Filter
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { View, Image, TouchableOpacity, Slider, Text, StyleSheet } from "react-native"
import { LinearGradient } from "expo-linear-gradient"
import React from "react"


const FilterDrawer = () => {

		return <View
				style={styles.filterView}>
				<View
					pointerEvents="box-none"
					style={{
						position: "absolute",
						left: 0,
						right: 0,
						top: 0,
						bottom: 0,
						justifyContent: "center",
					}}>
					<View
						style={styles.rectangleView}/>
				</View>
				<View
					pointerEvents="box-none"
					style={{
						position: "absolute",
						right: 0,
						top: 0,
						bottom: 0,
						justifyContent: "center",
					}}>
					<View
						style={styles.backgroundView}/>
				</View>
				<View
					pointerEvents="box-none"
					style={{
						position: "absolute",
						left: 83,
						right: -90,
						top: 124,
						bottom: 145,
						alignItems: "flex-end",
					}}>
					<View
						style={styles.carcategoryView}>
						<View
							pointerEvents="box-none"
							style={{
								width: 80,
								height: 79,
								alignItems: "flex-start",
							}}>
							<Text
								style={styles.carTypeText}>Car Type</Text>
							<TouchableOpacity
								onPress={this.onUnchooseBGThreePressed}
								style={styles.unchooseBgThreeButton}>
								<Text
									style={styles.unchooseBgThreeButtonText}>Sports</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity
							onPress={this.onUnchooseBGTwoPressed}
							style={styles.unchooseBgTwoButton}>
							<Text
								style={styles.unchooseBgTwoButtonText}>Exclusive</Text>
						</TouchableOpacity>
						<View
							style={{
								flex: 1,
							}}/>
						<TouchableOpacity
							onPress={this.onUnchooseBGPressed}
							style={styles.unchooseBgButton}>
							<Text
								style={styles.unchooseBgButtonText}>SUV</Text>
						</TouchableOpacity>
					</View>
					<View
						style={styles.brandView}>
						<View
							pointerEvents="box-none"
							style={{
								width: 80,
								height: 79,
								alignItems: "flex-start",
							}}>
							<Text
								style={styles.brandText}>Brand</Text>
							<TouchableOpacity
								onPress={this.onUnchooseBGSevenPressed}
								style={styles.unchooseBgFiveButton}>
								<Text
									style={styles.unchooseBgFiveButtonText}>Honda</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity
							onPress={this.onUnchooseBGSixPressed}
							style={styles.unchooseBgFourButton}>
							<Text
								style={styles.unchooseBgFourButtonText}>Toyota</Text>
						</TouchableOpacity>
						<View
							style={{
								flex: 1,
							}}/>
						<TouchableOpacity
							onPress={this.onChooseBGTwoPressed}
							style={styles.chooseBgButton}>
							<Text
								style={styles.chooseBgButtonText}>Kia</Text>
						</TouchableOpacity>
					</View>
					<View
						style={styles.motorconditionView}>
						<View
							pointerEvents="box-none"
							style={{
								width: 105,
								height: 79,
								alignItems: "flex-start",
							}}>
							<Text
								style={styles.conditionText}>Condition</Text>
							<TouchableOpacity
								onPress={this.onUnchooseBGFivePressed}
								style={styles.unchooseBgSevenButton}>
								<Text
									style={styles.unchooseBgSevenButtonText}>Verified New</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity
							onPress={this.onChooseBGPressed}
							style={styles.chooseBgTwoButton}>
							<Text
								style={styles.chooseBgTwoButtonText}>New</Text>
						</TouchableOpacity>
						<View
							style={{
								flex: 1,
							}}/>
						<TouchableOpacity
							onPress={this.onUnchooseBGFourPressed}
							style={styles.unchooseBgSixButton}>
							<Text
								style={styles.unchooseBgSixButtonText}> Belgium</Text>
						</TouchableOpacity>
					</View>
					<View
						style={styles.priceRangeView}>
						<Text
							style={styles.priceRangeText}>Price Range</Text>
						<View
							style={{
								flex: 1,
							}}/>
						<View
							style={styles.rangeBarView}>
							<View
								pointerEvents="box-none"
								style={{
									width: 70,
									height: 30,
									marginLeft: 35,
								}}>
								<View
									style={styles.priceAmountView}>
									<Text
										style={styles.textText}>₦</Text>
								</View>
								<Image
									source={require("./../../assets/images/triangle.png")}
									style={styles.triangleImage}/>
							</View>
							<View style={styles.sliderbarView}>
								<View style={styles.rectangle2View}/>
								<LinearGradient
									start={{ x: 0.18, y: 0.5, }}
									end={{ x: 0.88, y: 0.5, }}
									locations={[0, 1]}
									colors={["rgb(84, 84, 84)", "rgb(19, 20, 22)"]}
									style={styles.rectangle1ViewLinearGradient}>
									<View style={styles.rectangle1View}/>
								</LinearGradient>
								<View style={styles.slidercircleView}/>
							</View>
						</View>
						<Slider
							minimumTrackTintColor="rgb(84, 84, 84)"
							maximumTrackTintColor="rgb(19, 20, 22)"
							thumbTintColor="rgb(19, 20, 22)"
							value={0.5}
							style={{...styles.sliderbarView, ...styles.rectangle1View }}/>
					</View>
					<View
						style={{
							flex: 1,
						}}/>
					<View
						pointerEvents="box-none"
						style={{
							alignSelf: "stretch",
							height: 50,
							marginLeft: 41,
							marginRight: 110,
							flexDirection: "row",
							alignItems: "flex-end",
						}}>
						<TouchableOpacity
							onPress={this.onResetPressed}
							style={styles.resetButton}>
							<Text
								style={styles.resetButtonText}>Reset</Text>
						</TouchableOpacity>
						<View
							style={{
								flex: 1,
							}}/>
						<TouchableOpacity
							onPress={this.onApplyPressed}
							style={styles.applyButton}>
							<Text
								style={styles.applyButtonText}>Apply</Text>
						</TouchableOpacity>
					</View>
				</View>
			</View>
	}

export default FilterDrawer

FilterDrawer.navigationOptions ={
    // headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, }, //removes visibility of default header

    header: null
}

const styles = StyleSheet.create({
	filterView: {
		backgroundColor: "white",
		flex: 1,
	},
	rectangleView: {
		backgroundColor: "rgb(51, 51, 51)",
		height: 812,
	},
	backgroundView: {
		backgroundColor: "white",
		borderRadius: 32,
		width: 303,
		height: 812,
	},
	carcategoryView: {
		backgroundColor: "transparent",
		width: 344,
		height: 79,
		marginRight: 31,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	carTypeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		backgroundColor: "transparent",
		opacity: 0.8,
	},
	unchooseBgThreeButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginTop: 20,
	},
	unchooseBgThreeButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgThreeButtonText: {
		color: "white",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	unchooseBgTwoButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginLeft: 8,
		marginTop: 39,
	},
	unchooseBgTwoButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgTwoButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	unchooseBgButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginRight: 88,
		marginTop: 39,
	},
	unchooseBgButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	brandView: {
		backgroundColor: "transparent",
		width: 344,
		height: 79,
		marginRight: 31,
		marginTop: 30,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	brandText: {
		backgroundColor: "transparent",
		opacity: 0.8,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	unchooseBgFiveButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgFiveButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginTop: 20,
	},
	unchooseBgFiveButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	unchooseBgFourButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgFourButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginLeft: 8,
		marginTop: 39,
	},
	unchooseBgFourButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	chooseBgButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginRight: 88,
		marginTop: 39,
	},
	chooseBgButtonText: {
		color: "white",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	chooseBgButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	motorconditionView: {
		backgroundColor: "transparent",
		width: 382,
		height: 79,
		marginTop: 30,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	conditionText: {
		backgroundColor: "transparent",
		opacity: 0.8,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		marginLeft: 7,
	},
	unchooseBgSevenButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 105,
		height: 40,
		marginTop: 20,
	},
	unchooseBgSevenButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	unchooseBgSevenButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	chooseBgTwoButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginLeft: 8,
		marginTop: 39,
	},
	chooseBgTwoButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	chooseBgTwoButtonText: {
		color: "white",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	unchooseBgSixButton: {
		backgroundColor: "rgb(228, 228, 228)",
		borderRadius: 20,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 80,
		height: 40,
		marginRight: 101,
		marginTop: 39,
	},
	unchooseBgSixButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	unchooseBgSixButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	priceRangeView: {
		backgroundColor: "transparent",
		width: 265,
		height: 92,
		marginRight: 110,
		marginTop: 40,
		alignItems: "flex-start",
	},
	priceRangeText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		backgroundColor: "transparent",
		opacity: 0.8,
	},
	rangeBarView: {
		backgroundColor: "transparent",
		alignSelf: "stretch",
		height: 30,
		marginBottom: 15,
		alignItems: "flex-start",
	},
	priceAmountView: {
		backgroundColor: "rgb(51, 51, 51)",
		borderRadius: 5,
		position: "absolute",
		left: 0,
		width: 70,
		top: 0,
		height: 24,
		justifyContent: "center",
	},
	textText: {
		color: "rgba(255, 255, 255, 0.95)",
		fontFamily: "Avenir-Medium",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		marginLeft: 14,
		marginRight: 15,
	},
	triangleImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		position: "absolute",
		left: 28,
		width: 15,
		top: 23,
		height: 7,
	},
	sliderbarView: {
		backgroundColor: "transparent",
		alignSelf: "stretch",
		height: 20,
		marginTop: 4,
	},
	rectangle2View: {
		backgroundColor: "rgb(51, 51, 51)",
		opacity: 0.1,
		borderRadius: 7,
		position: "absolute",
		left: 0,
		right: 0,
		top: 3,
		height: 14,
	},
	rectangle1ViewLinearGradient: {
		borderRadius: 3,
		position: "absolute",
		left: 5,
		right: 5,
		top: 7,
		height: 6,
	},
	rectangle1View: {
		width: "100%",
		height: "100%",
	},
	slidercircleView: {
		backgroundColor: "rgb(51, 51, 51)",
		borderRadius: 10,
		shadowColor: "rgba(0, 0, 0, 0.21)",
		shadowRadius: 16,
		shadowOpacity: 1,
		position: "absolute",
		left: 60,
		width: 20,
		top: 0,
		height: 20,
	},
	sliderSlider: {
		backgroundColor: "transparent",
		width: 265,
		height: 31,
		marginTop: 15,
	},
	resetButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	resetButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 41,
		height: 19,
		marginBottom: 15,
	},
	resetButtonText: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
	applyButton: {
		backgroundColor: "rgb(19, 20, 22)",
		borderRadius: 25,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 132,
		height: 50,
	},
	applyButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	applyButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
	},
})
