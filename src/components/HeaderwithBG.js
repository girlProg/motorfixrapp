import React from 'react'
import {Image, View, Text, StyleSheet} from 'react-native'

const HeaderwithBG = () => {
      return (<View style={{alignItems: "center"}}>
           <Image
            source={require('../../assets/images/header-bg.png')}
            style={{ width: 200, height: 70 }}
            />
          <Text style={styles.placeNameText}>Chucka Motors</Text>
      </View>
           
      );
    }
  
export default HeaderwithBG

const styles = StyleSheet.create({
    placeNameText: {
		color: "rgb(240, 232, 229)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		letterSpacing: 0.37,
		backgroundColor: "transparent",
        alignSelf: "center",
        marginBottom: 20,
	},
})
  