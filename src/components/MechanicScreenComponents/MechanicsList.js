import React, { Component } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity } from "react-native";
import MechanicOnMap from "./MechanicOnMap";
import MapsCloseButton from "./MapsCloseButton";

function MechanicsList(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View>
        <ScrollView contentContainerStyle={styles.rect3}>
          <MechanicOnMap style={styles.mechanicOnMap}></MechanicOnMap>
        </ScrollView>
      </View>
      <TouchableOpacity>
        <MapsCloseButton style={styles.mapsCloseButton}></MapsCloseButton>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  rect3: {
    height: 521,
    backgroundColor: "rgba(255,255,255,1)",
    borderRadius: 8
  },
  mechanicOnMap: {
    width: 286,
    height: 94,
    backgroundColor: "rgba(255,255,255,1)",
    marginTop: 32,
    alignSelf: "center"
  },
  mapsCloseButton: {
    width: 56,
    height: 56,
    marginTop: 29,
    alignSelf: "center"
  }
});

export default MechanicsList;
