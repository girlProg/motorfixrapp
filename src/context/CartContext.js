import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import FixrApi from '../api/motorFixr';
import { navigate } from '../navigationRef';

const cartReducer = (state, action) => {
  switch (action.type) {
    case 'get_cart_items':
      return  {...state,  ...action.payload[0]} 
    case 'add_to_cart':
      return {...state, ...action.payload }
    case 'delete_from_cart':
      return { ...state, cartItems: [...state.cartItems.filter((item)=>{item.id !== action.payload.id})] };
    default:
      return state;
  }
};

const tryLocalSignin = dispatch => async () => {
  const token = await AsyncStorage.getItem('token');
  if (token) {
    dispatch({ type: 'signin', payload: token });
    navigate('TrackList');
  } else {
    navigate('Signup');
  }
};

const getCartItems = dispatch => async () => {
  const token = await AsyncStorage.getItem('token');
  // if (token) {
  if (true) {
    const response = await FixrApi.get('/cart', { token });
    dispatch({ type: 'get_cart_items' , payload: response.data.results});
    
  } else {
    //dispatch({ type: 'get_cart_items' , payload: {} });
  }
}

const deleteCartItem = dispatch => async (id) => {
  dispatch({ type: 'delete_from_cart' , payload: id});
};

const addToCart = dispatch => async (product) => {
  const response = await FixrApi.post(`/cart/`, { id : product.id });
  response.data.success ?  dispatch({ type: 'add_to_cart' , payload: response.data.cart }) : null
};


export const { Provider, Context } = createDataContext(
  cartReducer,
  { addToCart, deleteCartItem, getCartItems },
  []
);
