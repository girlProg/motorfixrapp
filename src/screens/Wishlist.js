import { StyleSheet, View, Text, FlatList, ActivityIndicator } from "react-native"
import React, {useContext, useEffect} from "react"
import ChosenCarHeader from "../components/ChosenCarHeader"
import BreadcrumbsNav from "../components/BreadcrumbsNav"
import WishListProductItem from "../components/WishListProductItem"
import ListingItem from "../components/ListingItem"
import { SafeAreaView, withNavigationFocus } from 'react-navigation'; 
import {Context as WishListContext} from '../context/WishListContext'


const Wishlist = ({navigation}) => {
	const {state, getWishList, getWishListIds} = useContext(WishListContext)
	useEffect(()=>{getWishList(); getWishListIds()}, [])
	// console.log(state.wishlistids)


	ListEmptyView = () => {
		return (
		<View style={{height:500,justifyContent:"center", alignSelf:"center"}}>
			<Text style={styles.descriptionText}>Add Items to Your Wishlist!</Text>
		</View>
	
		);
	  }


    return <>
        <SafeAreaView >
            <BreadcrumbsNav styles={{marginBottom: 15}} navigation={navigation} screenName={'Wishlist'}/>
			{state.products? state.products.length > 0 ? 
			
			<FlatList 
				data={state.products}
				keyExtractor = {item => item.id.toString()}
				refreshing = {false}
				onRefresh={getWishList}
				renderItem = {({item}) => {
					return <WishListProductItem navigation={navigation} product={item} />
				}}
				ListEmptyComponent={ListEmptyView }
			/>  : <View style={{height:500,justifyContent:"center", alignSelf:"center"}}>
			<Text style={styles.descriptionText}>Add Items to Your Wishlist!</Text>
		</View>   :  <ActivityIndicator/>
			}

			<View style={{flexDirection: "column"}}>
                
                {/* <ListingItem/> */}

            </View>
            
        </SafeAreaView>
            </>
}

export default Wishlist

Wishlist.navigationOptions =  {
	header: <ChosenCarHeader/>,
}

const styles = StyleSheet.create({
	productDetailsView: {
		backgroundColor: "white",
		flex: 1,
	},
	descriptionText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 0.2,
		marginLeft: 30,
		marginRight: 30,
		// marginTop: 191,
    }
})