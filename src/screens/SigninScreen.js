import React, { useContext, useState } from 'react';
import { TextInput, StyleSheet, TouchableOpacity, View, Image, Text  } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';
import { Context } from '../context/AuthContext';

const SigninScreen = ({navigation}) => {
  const { state, signin, clearErrorMessage } = useContext(Context);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  return <View style={styles.signInView}>
      <NavigationEvents onWillBlur={clearErrorMessage} />
				
		<Text style={styles.signInToYourAccoText}>Sign in to your account</Text>
		<View style={styles.usernameView}>
			<TextInput
				value={email}
				onChangeText={setEmail}
				autoCorrect={false}
				placeholder="Username"
				clearButtonMode="always"
				style={styles.usernameTextInput}/>
		</View>
		<View style={styles.passwordView}>
			<TextInput
				value={password}
				onChangeText={setPassword}
				autoCorrect={false}
				placeholder="Password"
				clearButtonMode="always"
				secureTextEntry={true}
				style={styles.passwordTextInput}/>
		</View>
        {state.errorMessage ? (
        <Text style={styles.errorMessage}>{state.errorMessage}</Text>
      ) : null}
		<TouchableOpacity
			onPress={signin}
			style={styles.signinbuttonButton}>
			<Text
				style={styles.signinbuttonButtonText}>Sign in</Text>
		</TouchableOpacity>

	</View>


      {/* <NavigationEvents onWillBlur={clearErrorMessage} />
      <AuthForm
        headerText="Sign In to Your Account"
        errorMessage={state.errorMessage}
        onSubmit={signin}
        submitButtonText="Sign In"
      />
      <NavLink
        text="Dont have an account? Sign up instead"
        routeName="Signup"
      /> 
    </View> */}
  
}


const styles = StyleSheet.create({
  errorMessage: {
    fontSize: 16,
    color: 'red',
    textAlign: "center",
    marginLeft: 15,
    marginTop: 15
  },
  signInView: {
		backgroundColor: "rgb(251, 252, 254)",
		flex: 1,
	},
	viewView: {
		backgroundColor: "transparent",
		height: 116,
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: null,
		height: 116,
	},
	motorfixrlogoView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 110,
		right: 109,
		top: 50,
		height: 36,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	iconImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		flex: 1,
		height: 36,
		marginRight: 9,
	},
	motorfixrText: {
		color: "white",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 3,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
	},
	fixingAllYourCarText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 24,
		top: 17,
	},
	signInToYourAccoText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 21,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		marginLeft: 80,
		marginRight: 80,
		marginTop: 99,
	},
	usernameView: {
		backgroundColor: "transparent",
		borderRadius: 30,
		borderWidth: 0.5,
		borderColor: "#dbd7ce",
		borderStyle: "solid",
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 25,
		justifyContent: "center",
		// alignItems: "flex-start",
	},
	usernameTextInput: {
		backgroundColor: "transparent",
		padding: 0,
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		// width: 269,
		height: 22,
    marginLeft: 20,
    marginRight: 14,
	},
	passwordView: {
		backgroundColor: "transparent",
		borderRadius: 30,
		borderWidth: 0.5,
		borderColor: "#dbd7ce",
		borderStyle: "solid",
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 10,
		justifyContent: "center",
	},
	passwordTextInput: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		height: 22,
		marginLeft: 20,
		marginRight: 14,
	},
	signinbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	signinbuttonButton: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 20,
	},
	signinbuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   marginBottom: 250
  // }
});

export default SigninScreen;
