
import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import CheckoutPagination from "../../components/CheckoutPagination"
import CartLineItem from "../../components/CartLineItem"
import MotorFixrButton from "../../components/MotorFixrButton"
import CartTotalSummary from "../../components/CartTotalSummary"
import { ScrollView } from 'react-native-gesture-handler';

const ReviewOrderScreen = ({ navigation }) => {

  return ( <>
      <ScrollView>
        <BreadcrumbsNav withbackArrow={true} navigation={navigation} screenName="Review Order"/>
        <CheckoutPagination/>
        <CartLineItem/>
        <CartTotalSummary/>
        <MotorFixrButton navigation={navigation} text="Proceed" destination="CartPayment" />
    </ScrollView>
        
    </>
  );
};

ReviewOrderScreen.navigationOptions = {
    header: <ChosenCarHeader/>,
    tabBarVisible: false,

};

const styles = StyleSheet.create({});

export default ReviewOrderScreen;
