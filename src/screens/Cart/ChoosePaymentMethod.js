import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import PaymentMethod from "../../components/PaymentMethod"
import MotorFixrButton from "../../components/MotorFixrButton"
import {SafeAreaView, NavigationActions} from "react-navigation"

const ChoosePaymentMethodScreen = ({ navigation }) => {
    
  return (
    <>
        <BreadcrumbsNav withbackArrow={false} navigation={navigation} screenName="Cart"/>
        <Text style={styles.addNewPaymentMethText}>Select payment method</Text>
        <PaymentMethod navigation={navigation} name="Debit Card" icon="./../../assets/images/rectangle-5.png" />
        <PaymentMethod navigation={navigation} name="PayStack" icon="./../../assets/images/bitmap-5.png" />
        <PaymentMethod navigation={navigation} name="Cash on Delivery" icon="./../../assets/images/bitmap-5.png" />
    </>
  );
};

ChoosePaymentMethodScreen.navigationOptions = {
    header: <ChosenCarHeader/>,
};

const styles = StyleSheet.create({
    addNewPaymentMethText: {
		color: "rgb(58, 44, 60)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		alignSelf: "flex-start",
		marginLeft: 8,
	},
});

export default ChoosePaymentMethodScreen;
