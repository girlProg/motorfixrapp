import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, Dimensions } from 'react-native';
import ChosenCarHeader from "../../components/ChosenCarHeader"
import BreadcrumbsNav from "../../components/BreadcrumbsNav"
import CheckoutPagination from "../../components/CheckoutPagination"


const OrderPlacedScreen = ({ navigation }) => {

  return (
    <>
    <BreadcrumbsNav withbackArrow={false} navigation={navigation} screenName="Order Placed"/>
     <CheckoutPagination/>
     {/* <View style={{justifyContent: "center"}}> */}
        <Image
            source={require("../../../assets/images/check.png")}    
            style={styles.checkImage}/>
     {/* </View> */}
      
        <Text style={styles.yourOrderHasBeenText}>Your order has been placed.{"\n"}You will receive a text and email confirmation shortly</Text>		
    </>
  );
};

OrderPlacedScreen.navigationOptions = {
    header: <ChosenCarHeader/>,
};

const styles = StyleSheet.create({
    checkImage: {
		resizeMode: "center",
		width: Dimensions.get("window").width,
		height: 142,
        marginTop: 53,
        alignContent: "center",
        
	},
	yourOrderHasBeenText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 12,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		letterSpacing: 0.28,
		marginTop: 47,
	},
});

export default OrderPlacedScreen;



