//
//  CreateUsername
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { TextInput, View, TouchableOpacity, Image, StyleSheet, Text } from "react-native"
import React from "react"
import LogoTitle from "../components/Logotitle"


const CreatePasswordScreen = ({navigation}) => {


    return <View style={styles.createUsernameView}>
            {/* <View pointerEvents="box-none"
                style={{
                    height: 116,
                }}>
                <View style={styles.headerBgView}>
                    <Image
                        source={require("./../../assets/images/header-bg.png")}
                        style={styles.headerBgImage}/>
                </View>
                <View style={styles.logoView}>
                    <Image source={require("./../../assets/images/group-2-4.png")}
                        style={styles.group2Image}/>
                    <View
                        style={{
                            flex: 1,
                        }}/>
                    <View
                        pointerEvents="box-none"
                        style={{
                            width: 118,
                            height: 33,
                            marginRight: 2,
                            marginTop: 1,
                        }}>
                        <Text style={styles.motorfixrText}>MOTORFIXR</Text>
                        <Text style={styles.fixingAllYourCarText}>Fixing all your car needs</Text>
                    </View>
                </View>
            </View> */}
            <Text
                style={styles.createpasswordtext}>Create password</Text>
            <View
                style={styles.eMailView}>
                <TextInput
                    clearButtonMode="always"
                    autoCorrect={false}
                    placeholder="Password"
                    secureTextEntry={true}
                    style={styles.usernameTextInput}/>
            </View>
            <View
                pointerEvents="box-none"
                style={{
                    height: 60,
                    marginLeft: 30,
                    marginRight: 30,
                    marginTop: 20,
                }}>
                <Text
                    style={styles.getStartedText}>Continue</Text>
                <TouchableOpacity
                    onPress={()=> navigation.navigate('EnterMobileNumber')}
                    style={styles.gobuttonButton}>
                    <Text
                        style={styles.gobuttonButtonText}>Continue</Text>
                </TouchableOpacity>
            </View>
        </View>
}

export default CreatePasswordScreen
CreatePasswordScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}



const styles = StyleSheet.create({
	createUsernameView: {
		backgroundColor: "rgb(251, 252, 254)",
		flex: 1,
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	logoView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 110,
		right: 109,
		top: 50,
		height: 36,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	group2Image: {
		resizeMode: "center",
		backgroundColor: "transparent",
		width: 27,
		height: 36,
	},
	motorfixrText: {
		backgroundColor: "transparent",
		color: "white",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 3,
		position: "absolute",
		right: 0,
		top: 0,
	},
	fixingAllYourCarText: {
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		backgroundColor: "transparent",
		position: "absolute",
		right: 24,
		top: 17,
	},
	createpasswordtext: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 21,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		marginLeft: 99,
		marginRight: 99,
		marginTop: 99,
	},
	eMailView: {
		backgroundColor: "transparent",
		borderRadius: 30,
		borderWidth: 0.5,
		borderColor: "white",
		borderStyle: "solid",
		height: 60,
		marginLeft: 30,
		marginRight: 30,
		marginTop: 25,
		borderColor: "#dbd7ce",
	},
	usernameTextInput: {
		color: "rgb(136, 136, 136)",
		fontFamily: "Avenir-Medium",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		backgroundColor: "transparent",
		padding: 0,
		height: 39,
		marginLeft: 18,
		marginRight: 18,
		marginTop: 12,
	},
	getStartedText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.32,
		backgroundColor: "transparent",
		position: "absolute",
		left: 111,
		right: 123,
		top: 15,
	},
	gobuttonButton: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		position: "absolute",
		alignSelf: "center",
		width: 315,
		top: 0,
		height: 60,
	},
	gobuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
	gobuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	signInButtonText: {
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
	},
	signInButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	signInButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		height: 19,
		marginLeft: 78,
		marginRight: 79,
		marginTop: 17,
	},
})
