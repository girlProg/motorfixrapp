import React, { useState } from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";
import MapView from "react-native-maps";
import MechanicsList from "../components/MechanicScreenComponents/MechanicsList";
import OnMapMechList from "../components/OnMapMechList";
import {SafeAreaView} from "react-navigation"
import Map from '../components/Map';

const NearbyMechanicScreen = ({ navigation }) => {
    const [isDetailView, setIsDetailView] = useState(false)

  return (
    <View style={styles.container}>
      <View style={styles.mapViewStack}>
        <MapView
          provider={MapView.PROVIDER_GOOGLE}
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }}
          customMapStyle={[]}
          style={styles.mapView}
        ></MapView>
        <View style={{top: (Dimensions.get('window').height/2),
                    // left: 32,
                    // width: 311,
                    // height: 294,
                    position: "absolute",
                    alignSelf: "center",
                    opacity: 1, }}>
            <OnMapMechList />
    
        </View>
        {isDetailView? <View style={styles.greyBackground}>
          <Text style={styles.nearbyMechanic}>Nearby Mechanics</Text>
          <MechanicsList style={styles.mechanicsList}></MechanicsList>
        </View> : null } 
        
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    
  container: {
    flex: 1
  },
  mapView: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  greyBackground: {
    top: 0,
    left: 0,
    backgroundColor: "rgba(45,45,45,0.9)",
    position: "absolute",
    right: 0,
    bottom: 0
  },
  nearbyMechanic: {
    color: "rgba(240,232,229,1)",
    fontSize: 20,
    fontFamily: "Avenir",
    marginTop: 96,
    alignSelf: "center"
  },
  mechanicsList: {
    width: 323,
    height: 606,
    marginTop: 23,
    alignSelf: "center"
  },
  mapViewStack: {
    flex: 1
  }
})

NearbyMechanicScreen.navigationOptions = {
//   title: 'Tracks'
    header: null,
}


export default NearbyMechanicScreen;

