//
//  MechanicVerified
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import { StyleSheet, View, Text, Image } from "react-native"
import React from "react"
import { TouchableOpacity } from "react-native-gesture-handler"
import LogoTitle from "../components/Logotitle"
import MotorFixrButton from "../components/MotorFixrButton"

const AccountVerifiedScreen = ({navigation}) => {

	
		return <View style={styles.mechanicVerifiedView}>
				<View style={styles.group2View}>
					<View style={styles.groupTwoView}>
						<Image
							source={require("./../../assets/images/check.png")}
							style={styles.checkImage}/>
						<View style={{ flex: 1, }}/>
						<Text style={styles.verifiedText}>Verified</Text>
					</View>
					<View style={{ flex: 1, }}/>
					
				</View>
                <TouchableOpacity onPress={()=> {navigation.navigate("Homepage")}}>
                <View style={styles.group7View}>
						<Text style={styles.getStartedText}>Continue</Text>
					</View>
                </TouchableOpacity>
			</View>
	}


export default AccountVerifiedScreen

AccountVerifiedScreen.navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    }
}

const styles = StyleSheet.create({
	mechanicVerifiedView: {
		backgroundColor: "white",
		flex: 1,
	},
	headerBgView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 0,
		right: 0,
		top: 0,
		height: 116,
		justifyContent: "center",
	},
	headerBgImage: {
		resizeMode: "cover",
		backgroundColor: "transparent",
		width: null,
		height: 116,
	},
	groupView: {
		backgroundColor: "transparent",
		position: "absolute",
		left: 110,
		right: 109,
		top: 50,
		height: 36,
		flexDirection: "row",
		alignItems: "flex-start",
	},
	group2Image: {
		resizeMode: "center",
		backgroundColor: "transparent",
		flex: 1,
		alignSelf: "center",
		height: 36,
		marginRight: 9,
	},
	motorfixrText: {
		backgroundColor: "transparent",
		color: "white",
		// fontFamily: ".AppleSystemUIFont",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		letterSpacing: 3,
		position: "absolute",
		right: 0,
		top: 0,
	},
	fixingAllYourCarText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Book",
		fontSize: 9,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		lineHeight: 16,
		position: "absolute",
		right: 24,
		top: 17,
	},
	group2View: {
		backgroundColor: "transparent",
		alignSelf: "center",
		width: 315,
		height: 284,
		marginTop: 141,
		alignItems: "center",
	},
	groupTwoView: {
		backgroundColor: "transparent",
		width: 264,
		height: 199,
		alignItems: "center",
	},
	checkImage: {
		backgroundColor: "transparent",
		resizeMode: "center",
		width: 148,
		height: 142,
	},
	verifiedText: {
		backgroundColor: "transparent",
		color: "rgb(6, 6, 6)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "center",
		letterSpacing: 0.2,
		alignSelf: "stretch",
		marginLeft: 107,
		marginRight: 107,
		marginBottom: 1,
	},
	group7View: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		alignSelf: "stretch",
		height: 60,
		justifyContent: "center",
        alignItems: "center",
        marginLeft: 40,
        marginRight: 40
	},
	getStartedText: {
		backgroundColor: "transparent",
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
		letterSpacing: 0.32,
	},
})
