//
//  SelectCarMakeCopy
//  Project
//
//  Created by girlProg.
//  Copyright © 2018 YediTech. All rights reserved.
//

import React, {useContext, useState} from "react"
import { TouchableOpacity, View, Text, TextInput, StyleSheet, Image, StatusBar } from "react-native"
import {Context as vinContext} from '../../context/VINAPIcontext'
import { StackActions } from 'react-navigation';

const AddVINScreen = ({navigation}) =>  {
	const {state, decodeVIN} = useContext(vinContext)
	const [vinEntered, setVinEntered] = useState('')
	
	const goHome = () => {
		const popAction = StackActions.pop({
			n: 2,
		  });
		  navigation.dispatch(popAction);
	}
		return <View style={styles.selectCarMakeCopyView}>
				<StatusBar barStyle="dark-content" />
				<View  style={{
						position: "absolute",
						left: 24,
						right: 24,
						top: 43,
						height: 227,
						alignItems: "center",
					}}>
					<View
						style={styles.vinnumberentryviewView}>
						<Image
							source={require("../../../assets/images/icon---search.png")}
							style={styles.iconSearchImage}/>
						<TextInput
							value={vinEntered}
							onChangeText={setVinEntered}
							autoCorrect={false}
							autoCapitalize="characters"
							placeholder="Add VIN number "
							style={styles.placeholderTextInput}/>
					</View>
					<TouchableOpacity
						// onPress={()=>{navigation.navigate("SelectCarMake")}}
						onPress={()=>{decodeVIN(vinEntered)}}
						style={styles.checkvinbuttonButton}>
						<Text
							style={styles.checkvinbuttonButtonText}>Check VIN</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={()=>navigation.navigate('SelectCarMake')}
						style={styles.donTHaveAVinButton}>
						<Text style={styles.donTHaveAVinButtonText}>Don’t have a VIN?</Text>
					</TouchableOpacity>
					{state.selectedCar && state.selectedCar.Make !== '' && goHome()}
					{state.selectedCar && state.selectedCar.Make === '' && <Text
							style={styles.donTHaveAVinButtonText}>Could not find matching car VIN</Text>}
				</View>
			</View>
	}

export default AddVINScreen

AddVINScreen.navigationOptions = ({navigation}) => {
    return{
        headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, }, //removes visibility of default header
        headerTitle: <Text style={styles.selectCarMakeText}>Add VIN Number</Text>,
	  	headerLeft: <TouchableOpacity onPress={()=>{navigation.navigate("Homepage")}}>
						<Text style={styles.cancelText}>Cancel</Text>
					</TouchableOpacity>
    }
}

const styles = StyleSheet.create({
	selectCarMakeCopyView: {
		backgroundColor: "white",
		flex: 1,
	},
	selectCarMakeText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		position: "absolute",
		alignSelf: "center",
		// top: 43,
	},
	cancelText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		alignSelf: "flex-start",
		marginLeft: 31,
	},
	vinnumberentryviewView: {
		backgroundColor: "white",
		borderRadius: 24,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		alignSelf: "stretch",
		height: 48,
		marginTop: 15,
		flexDirection: "row",
		alignItems: "center",
		// justifyContent: "flex-start"
	},
	iconSearchImage: {
		resizeMode: "center",
		backgroundColor: "transparent",
		flex: 1,
		height: 24,
		marginLeft: 10,
		marginRight: 10,
	},
	placeholderTextInput: {
		backgroundColor: "transparent",
		opacity: 1,
		padding: 0,
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Book",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		width: "80%",
		height: 20,
		marginLeft: 16,
	},
	checkvinbuttonButton: {
		backgroundColor: "rgb(23, 24, 26)",
		borderRadius: 30,
		shadowColor: "rgba(0, 0, 0, 0.08)",
		shadowRadius: 8,
		shadowOpacity: 1,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: '100%',
		height: 60,
		marginTop: 45,
	},
	checkvinbuttonButtonText: {
		color: "white",
		fontFamily: "Avenir-Heavy",
		fontSize: 14,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "center",
	},
	checkvinbuttonButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
	donTHaveAVinButtonText: {
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
	},
	donTHaveAVinButton: {
		backgroundColor: "transparent",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		padding: 0,
		width: 292,
		height: 22,
		marginTop: 15,
	},
	donTHaveAVinButtonImage: {
		resizeMode: "contain",
		marginRight: 10,
	},
})
