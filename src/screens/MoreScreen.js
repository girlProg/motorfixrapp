import React from 'react';
import { StyleSheet, View, Image, Text, FlatList, TouchableOpacity } from 'react-native';
import Svg, { Path } from "react-native-svg";

const MoreScreen = ({ navigation }) => {

  return (
    <>
       <View style={styles.container}>
      <View style={styles.rectangle3Stack}>
        <View style={styles.rectangle3}>
            
            <TouchableOpacity onPress={() => navigation.navigate("CreateUsername")}>
            <Text style={styles.signInRegister}>Sign In / Register</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate("CreateAProfile")}>
            <Text style={styles.registerAsASeller}>Register as a Seller</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate("CreateAProfile")}>
            <Text style={styles.registerAsAMechan}>Register as a Mechanic</Text>
            </TouchableOpacity>

            <TouchableOpacity>
            <Text style={styles.about}>About</Text>
            </TouchableOpacity>
           
        </View>
        <Svg viewBox="-1 -1 378 6" style={styles.lineCopy2}>
          <Path
            strokeWidth={2}
            fill="rgba(251,252,254,1)"
            stroke="rgba(251,252,254,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M1.00 2.00 L375.00 2.00 "
          ></Path>
        </Svg>
        <Svg viewBox="-1 -1 378 6" style={styles.lineCopy4}>
          <Path
            strokeWidth={2}
            fill="transparent"
            stroke="rgba(251,252,254,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M1.00 2.00 L375.00 2.00 "
          ></Path>
        </Svg>
        <Svg viewBox="-1 -1 378 6" style={styles.lineCopy5}>
          <Path
            strokeWidth={2}
            fill="transparent"
            stroke="rgba(251,252,254,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M1.00 2.00 L375.00 2.00 "
          ></Path>
        </Svg>
        <Svg viewBox="-1 -1 378 6" style={styles.lineCopy7}>
          <Path
            strokeWidth={2}
            fill="transparent"
            stroke="rgba(251,252,254,1)"
            fillOpacity={1}
            strokeOpacity={1}
            d="M1.00 2.00 L375.00 2.00 "
          ></Path>
        </Svg>
      </View>
    </View>
    </>
  );
};

MoreScreen.navigationOptions=({navigation}) => {
    return {
        headerStyle: { shadowColor: 'transparent', borderBottomWidth: 0, }, //removes visibility of default header
        headerTitle: <Text style={styles.selectCarMakeText}>More</Text>,
	  	headerLeft: <TouchableOpacity onPress={()=>{navigation.navigate("Homepage")}}>
						<Text style={styles.cancelButton}>Cancel</Text>
					</TouchableOpacity>
    }
}


const styles = StyleSheet.create({
    selectCarMakeText: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Heavy",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "bold",
		textAlign: "left",
		position: "absolute",
		alignSelf: "center",
		// top: 43,
	},
	cancelButton: {
		backgroundColor: "transparent",
		color: "rgb(51, 51, 51)",
		fontFamily: "Avenir-Roman",
		fontSize: 16,
		fontStyle: "normal",
		fontWeight: "normal",
		textAlign: "left",
		alignSelf: "flex-start",
		marginLeft: 31,
	},
    container: {
      width: 378,
      height: 243
    },
    rectangle3: {
      top: 0,
      left: 1,
      width: 375,
      height: 240,
      backgroundColor: "rgba(255,255,255,1)",
      position: "absolute"
    },
    signInRegister: {
      backgroundColor: "transparent",
      color: "rgba(51,51,51,1)",
      opacity: 1,
      fontSize: 16,
      fontFamily: "Avenir-Medium",
      letterSpacing: 0.37,
      marginTop: 19,
      marginLeft: 60
    },
    registerAsASeller: {
      backgroundColor: "transparent",
      color: "rgba(51,51,51,1)",
      opacity: 1,
      fontSize: 16,
      fontFamily: "Avenir-Medium",
      letterSpacing: 0.37,
      marginTop: 43,
      marginLeft: 60
    },
    registerAsAMechan: {
      backgroundColor: "transparent",
      color: "rgba(51,51,51,1)",
      opacity: 1,
      fontSize: 16,
      fontFamily: "Avenir-Medium",
      letterSpacing: 0.37,
      marginTop: 44,
      marginLeft: 60
    },
    about: {
      backgroundColor: "transparent",
      color: "rgba(51,51,51,1)",
      opacity: 1,
      fontSize: 16,
      fontFamily: "Avenir-Medium",
      letterSpacing: 0.37,
      marginTop: 44,
      marginLeft: 60
    },
    lineCopy2: {
      top: 57,
      left: 0,
      height: 6,
      backgroundColor: "transparent",
      position: "absolute",
      borderColor: "transparent",
      right: 0
    },
    lineCopy4: {
      top: 117,
      left: 0,
      height: 6,
      backgroundColor: "transparent",
      position: "absolute",
      borderColor: "#fbfcfe",
      right: 0
    },
    lineCopy5: {
      top: 177,
      left: 0,
      height: 6,
      backgroundColor: "transparent",
      position: "absolute",
      borderColor: "transparent",
      right: 0
    },
    lineCopy7: {
      top: 237,
      left: 0,
      height: 6,
      backgroundColor: "transparent",
      position: "absolute",
      borderColor: "transparent",
      right: 0
    },
    rectangle3Stack: {
      height: 243
    }
  });
export default MoreScreen;

