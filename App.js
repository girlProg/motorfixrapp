import React , {useEffect} from 'react';
import {
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator
} from 'react-navigation';
import {StatusBar, Image} from 'react-native'
// import { createDrawerNavigator } from 'react-navigation-drawer';
// import { createBottomTabNavigator } from 'react-navigation-tabs';

import AccountScreen from './src/screens/AccountScreen';
import SigninScreen from './src/screens/SigninScreen';
import SignupScreen from './src/screens/SignupScreen';
import TrackCreateScreen from './src/screens/TrackCreateScreen';
import TrackDetailScreen from './src/screens/TrackDetailScreen';
import TrackListScreen from './src/screens/TrackListScreen';

import OnBoardingScreen from './src/screens/OnBoardingScreen';
import PickAServiceScreen from './src/screens/PickAServiceScreen';
import CreateUsernameScreen from './src/screens/CreateUsernameScreen';
import CreatePasswordScreen from './src/screens/CreatePasswordScreen';
import EnterMobileNumberScreen from './src/screens/EnterMobileNumberScreen';
import EnterVerificationCodeScreen from './src/screens/EnterVerificationCodeScreen';
import AccountVerifiedScreen from './src/screens/AccountVerifiedScreen';
import Homepage from './src/screens/Homepage';
import MechanicDetails from './src/screens/MechanicDetails';
import NearbyMechanicScreen from './src/screens/NearbyMechanicScreen';
import CreateAProfileScreen from './src/screens/CreateAProfile';


import AddCarStepOne from './src/screens/AddCarStepOne';
import AddVINScreen from './src/screens/CarSelection/AddVINScreen';
import SelectCarMake from './src/screens/CarSelection/SelectCarMake';
import CategoryProductsScreen from './src/screens/CategoryProductsScreen';
import FilterDrawer from './src/components/FilterDrawer';
import ProductDetail from './src/screens/ProductDetail';
import MoreProductInfoScreen from './src/screens/ProductDetailThings/MoreProductInfo';
import ProductReviewScreen from './src/screens/ProductDetailThings/ProductReviewScreen';
import MechanicsOnMap from './src/screens/MechanicsOnMap';
import Wishlist from './src/screens/Wishlist';
import MoreScreen from './src/screens/MoreScreen';
import BusinessInformationScreen from './src/screens/BusinessInformationScreen';

import CartScreen from './src/screens/Cart/CartScreen';
import CartShippingDetails from './src/screens/Cart/ShippingDetailsScreen';
import ReviewOrderScreen from './src/screens/Cart/ReviewOrderScreen';
import CartPaymentScreen from './src/screens/Cart/PaymentScreen'
import OrderPlacedScreen from './src/screens/Cart/OrderPlacedScreen';
import ChoosePaymentMethod from './src/screens/Cart/ChoosePaymentMethod';



import { Provider as AuthProvider } from './src/context/AuthContext';
import { setNavigator } from './src/navigationRef';
import ResolveAuthScreen from './src/screens/ResolveAuthScreen';
import { Provider as LocationProvider } from './src/context/LocationContext';
import { Provider as TrackProvider } from './src/context/TrackContext';
import { Provider as CartProvider } from './src/context/CartContext';
import { Provider as CategoryProvider } from './src/context/CategoryContext';
import { Provider as ProductProvider } from './src/context/ProductContext';
import { Provider as SellerProvider } from './src/context/SellerContext';
import { Provider as WishListProvider } from './src/context/WishListContext';
import { Provider as VINProvider } from './src/context/VINAPIcontext';

import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import LogoTitle from './src/components/Logotitle'
import BackButton from './src/components/BackButton'
import TabBar from "./src/components/TabBar"
import SellerTabBar from "./src/components/SellerTabBar"
import Menu from "./src/components/SVGTabBar"
import HomeTab from "./src/components/Tabs/HomeTab"
import MechanicTabBar from "./src/components/MechanicTabBar"



const trackListFlow = createStackNavigator({
  TrackList: TrackListScreen,
  TrackDetail: TrackDetailScreen
});

trackListFlow.navigationOptions = {
  title: 'Tracks',
  tabBarIcon: <FontAwesome name="th-list" size={20} />
};




//mechanic tab stack
// const mhomeFlow = createStackNavigator({
//   home : Homepage,
// })





//shopper tab stack
const moreFlow = createStackNavigator({
  MoreScreen : MoreScreen,
  CreateUsername : CreateUsernameScreen,
  CreateAProfile: CreateAProfileScreen,
  BusinessInformation : BusinessInformationScreen,
  EnterMobileNumber : EnterMobileNumberScreen,
  EnterVerificationCode : EnterVerificationCodeScreen,
  AccountVerified : AccountVerifiedScreen,
  Signin: SigninScreen,
  CreatePassword : CreatePasswordScreen,
})
const nearbyFlow = createStackNavigator({
  NearbyMechanic : NearbyMechanicScreen,
  MechanicDetails : MechanicDetails,
})
const wishlistFlow = createStackNavigator({
    Wishlist : Wishlist,
    ProductDetail :ProductDetail,
    // Cart : CartScreen,
})
const CartFlow = createStackNavigator({
  Cart : CartScreen,
  ProductDetail :ProductDetail,
  ShippingDetails: CartShippingDetails,
  ReviewOrder: ReviewOrderScreen,
  ChoosePaymentMethod: ChoosePaymentMethod,
  CartPayment: CartPaymentScreen,
  OrderPlaced: OrderPlacedScreen,
})
const CartFlow2 = createStackNavigator({
  // Cart : CartScreen,
  ShippingDetails: CartShippingDetails,
  ReviewOrder: ReviewOrderScreen,
  CartPayment: CartPaymentScreen,
  OrderPlaced: OrderPlacedScreen,
})
CartFlow2.navigationOptions = {
    tabBarVisible: false,
}
const homeFlow = createStackNavigator({
  Homepage : Homepage,
  AddCarStepOne : AddCarStepOne,
  AddVIN : AddVINScreen,
  SelectCarMake : SelectCarMake,
  ProductDetail :ProductDetail,
  FilterDrawer: FilterDrawer,
  CategoryProducts: CategoryProductsScreen,
  MoreProductInfo: MoreProductInfoScreen,
  ProductReviews: ProductReviewScreen})
  
  homeFlow.navigationOptions = {
    // tabBarButtonComponent: HomeTab,
    // tabBarIcon: ({ focused,tintColor }) => (
    //   focused ? <Image source={require('./assets/images/home.png')} /> : <Image source={require('./assets/images/home.png')} />
    // ),
  }

const MainTabNavigator = createBottomTabNavigator({
  Home: { screen: homeFlow ,},
  Cart: {screen: CartFlow },
  More: {screen: moreFlow},
  Nearby: {screen: nearbyFlow },
  
  Wishlist: { screen: wishlistFlow },
  
  // Cart2: {screen: CartFlow2 },
  
}, {
  // tabBarComponent: TabBar,
  // tabBarComponent: SellerTabBar,
  tabBarComponent: Menu,
  resetOnBlur: true,

});


const MechanicTabStack = createBottomTabNavigator({
  home: {screen: homeFlow },
}, { tabBarComponent: MechanicTabBar, })


const switchNavigator = createSwitchNavigator({
  ResolveAuth: ResolveAuthScreen,
  // loginFlow: createStackNavigator({
  //   Signup: SignupScreen,
  //   Signin: SigninScreen
  // }),
  
  
  

  mainFlow: createStackNavigator({
    Cart : CartScreen,
    Wishlist : Wishlist,
    Homepage : Homepage,
    AddCarStepOne : AddCarStepOne,
    AddVIN : AddVINScreen,
    ProductDetail :ProductDetail,
    FilterDrawer: FilterDrawer,
    CategoryProducts: CategoryProductsScreen,
    MoreProductInfo: MoreProductInfoScreen,
    ProductReviews: ProductReviewScreen,

    
    MechanicDetails : MechanicDetails,
    MechanicsOnMap : MechanicsOnMap,
  
    
    SelectCarMake : SelectCarMake,
  
    
  }),

  newbieFlow: createStackNavigator({  
    Onboard : OnBoardingScreen,
    PickAService : PickAServiceScreen,
    CreateUsername : CreateUsernameScreen,
    Signin: SigninScreen,
    AccountVerified : AccountVerifiedScreen,
    EnterVerificationCode : EnterVerificationCodeScreen,
    EnterMobileNumber : EnterMobileNumberScreen,
    CreatePassword : CreatePasswordScreen,

  }, {
    defaultNavigationOptions:{
      headerTitle: <LogoTitle />,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#131416',
      height : 120,
    },
  // headerLeft: <BackButton/> 
    headerBackTitle: ' '
    }
  })
}, {
  initialRouteName: 'mainFlow'
});

// const App = createAppContainer(MechanicTabStack);
const App = createAppContainer(MainTabNavigator);
// const App = createAppContainer(CartFlow);
// const App = createAppContainer(switchNavigator);


// useEffect(()=>{
//     Font.loadAsync({
//       'avenir': require('./assets/fonts/Avenir-Medium.ttf'),
//     })
//   }
//   , [])

export default () => {
  return (
    <VINProvider>
    <WishListProvider>
    <SellerProvider>
    <ProductProvider>
    <CategoryProvider>
    <CartProvider>
    <TrackProvider>
      <LocationProvider>
        <AuthProvider>
        <StatusBar barStyle="light-content" />
          <App
            ref={navigator => {
              setNavigator(navigator);
            }}
          />
        </AuthProvider>
      </LocationProvider>
    </TrackProvider>
    </CartProvider>
    </CategoryProvider>
    </ProductProvider>
    </SellerProvider>
    </WishListProvider>
    </VINProvider>
  );
};
